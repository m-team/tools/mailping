Installation
==============

MailPing Environment Setup
****************************

**msmtp**

Install and configure *msmtp* smtp client or any other smtp client of your choice in order to send emails.

**postfix**

Install the *postfix* server. *Postfix* will receive and pipe incoming emails (responses from security contacts)
into MailPing python script. For that, include following setting into *postfix* configuration file (*/etc/postfix/main.cf*)::

   mailbox_command = sudo /usr/local/bin/process_email "$SENDER"

Then give sudo rights to execute *process_email* script for your postfix user, i.e.::

    File /etc/sudoers

    ...
    scc-mailping-0001 ALL= NOPASSWD:/usr/local/bin/process_email

*/usr/local/bin/process_email* script will be installed together with MailPing (see next section).

**mutt**

MailPing uses *mutt* email client in a batch-mode for sending signed emails. By default, the security features like
signing and encrypting emails are disabled in the batch-mode. To overcome this restrictions, you need to modify
the *send.c* file from *mutt* sources as follows (*mutt* version 1.14.6):

Search for the following line in *send.c*::

    if (WithCrypto && (sctx->msg->security == 0) && !(sctx->flags & (SENDBATCH | SENDMAILX | SENDPOSTPONED | SENDRESEND)))

Remove the "SENDBATCH" from there, so the modified line will be::

    if (WithCrypto && (sctx->msg->security == 0) && !(sctx->flags & (SENDMAILX | SENDPOSTPONED | SENDRESEND)))

After that you can build the *mutt* from your modified sources with gpgme support (*--enable-gpgme*).

Once the mutt has been installed, configure it. The configuration file *.muttrc* looks like::

    set sendmail="/usr/bin/msmtp"
    set use_from=yes
    set realname="Sirtfi Mail Ping"
    set from=mailping-0001@scc.kit.edu
    set envelope_from=yes
    set smime_sign_as="0x89FD186C"
    set crypt_use_gpgme = yes

Import your S/MIME certificate in order to use it for email signing in S/MIME mode. Also import your PGP key
to be able to send PGP/MIME or PGP/Inline Signed emails.

**Apache 2**

In order to serve the MailPing website, install the Apache HTTP Server. Apache will serve the static content (e.g. docs),
dynamic content with ReactJS, act as a proxy for API requests to *gunicorn*, and provide authorisation
by using *mod_auth_openidc*.

Create new virtual host for the MailPing website, i.e. /etc/apache2/sites-available/mailping.data.kit.edu.conf

Enable Apache proxying by::

    a2enmod proxy_http

If you want to secure the website with OpenID Connect, install *mod_auth_opeindc*::

    apt-get install libapache2-mod-auth-openidc
    a2enmod auth_openidc

Here is the Apache configuration of the MailPing host (*/etc/apache2/sites-available/mailping.data.kit.edu.conf*)::

    # Redirect HTTP requests to HTTPs
    <VirtualHost *:80>
        ServerName mailping.data.kit.edu
        Redirect / https://mailping.data.kit.edu
    </VirtualHost>

    <VirtualHost *:443>
        ServerAdmin m-ops@lists.kit.edu
        # Directory where your website exists
        DocumentRoot /var/www/mailping
        ServerName mailping.data.kit.edu

        # Rewrite rules for ReactJS
        <Directory /var/www/mailping>
          RewriteEngine On
              RewriteBase /
              RewriteRule ^index\.html$ - [L]
              RewriteCond %{REQUEST_FILENAME} !-f
              RewriteCond %{REQUEST_FILENAME} !-d
              RewriteCond %{REQUEST_FILENAME} !-l
              RewriteRule . /index.html [L]
              Order allow,deny
              allow from all
              Require all granted
        </Directory>

        # Redirect to the main page
        RedirectMatch ^/$ /campaigns/all

        # Enable SSL encryption
        SSLEngine on
        SSLCertificateFile /path/to/cert
        SSLCertificateKeyFile /path/to/key
        SSLCertificateChainFile /path/to/pem

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # ProxyPass to gunicorn
        ProxyPass / !
        <Location /api/>
            # Secure /api/ calls with OpenID Connect authorisation
            AuthType openid-connect
            Require claim eduperson_entitlement:urn:geant:h-df.de:group:m-team#login.helmholtz.de
            ProxyPass http://localhost:8080/api/
            ProxyPassReverse http://localhost:8080/api/
        </Location>
        <Location /public-api/>
            ProxyPass http://localhost:8080/public-api/
            ProxyPassReverse http://localhost:8080/public-api/
        </Location>

        # Configure OpenIDConnect
        OIDCProviderMetadataURL https://login.helmholtz.de/oauth2/.well-known/openid-configuration
        OIDCClientID clientid
        OIDCClientSecret password
        OIDCRedirectURI https://mailping.data.kit.edu/campaigns/redirect_uri
        OIDCCryptoPassphrase passphrase
        OIDCCookie mailping_openidc_session
        OIDCScope "openid profile"

        # secure /campaigns/ pages with OpenID Connect authorisation
        <Location /campaigns/>
          AuthType openid-connect
          Require claim eduperson_entitlement:urn:geant:h-df.de:group:m-team#login.helmholtz.de
        </Location>
    </VirtualHost>

    # vim: syntax=apache ts=4 sw=4 sts=4 sr noet


MailPing Deployment
*********************
To install the MailPing clone the *mailping* repository to your host where you want to run
the MailPing. Then follow this steps:

1. Build the *sirtfi_mailping* package with *setuptools* using following command::

    python3 setup.py sdist

The build will be stored in the dist/ folder.

2. Install the *sirtfi_mailping* package using pip::

    pip install dist/...tar.gz

The package will be installed into dist-packages folder of your python installation. Entry-points for mailping and reping
commands will be also created.

3. Move the empty SQLite MailPing database from *sirtfi_mailping/core/database/mailping.db* into
*/var/lib/sirtfi_mailping/mailping.db*.

4. Start serving API requests i.e. by using local WSGI *gunicorn* server. Execute following command::

    gunicorn sirtfi_mailping.api_application:app --bind localhost:8080 --worker-class aiohttp.GunicornWebWorker --daemon

To stop the *gunicorn* instance, execute::

    pkill gunicorn

Apache will provide a ProxyPass to the local gunicorn server in order to make the api available outside of the local host.


Website Deployment
*********************

To deploy the MailPing website, clone the *mailping-web* repository to your host.

Build the frontend from sources using *npm*::

    apt install npm
    npm install
    npm run build

Then move the content of the build folder into your website folder (i.e. */var/www/mailping/*).

**Build documentation**

To build MailPing documentation with *sphinx*, run the following command inside of the mailping/doc folder from sources::

    sphinx-build -b html source/ build

Then move the content of build folder into your website (sub)folder (i.e. */var/www/mailping/docs/*)



