Workflow
==============


**Create campaign**

Before creating the 'ping' campaign, make sure you have a list of security contacts that you want to ping.
Store the list in the following form as a text file::

    service_id_1,address_1,...,address_n
    ...
    service_id_x,address_1,...,address_m

whereas **service_id** is an int value that identifies the service provider id (user-defined), and **address** is the
email address of security contact that is responsible for the service.

All addresses will be checked to allow only valid email addresses.

Once you have the file with addresses ready, run the following command::

    mailping

You will be prompted to enter the following information:

    - path to contacts file
    - name of the newly created campaign
    - signing method (PGP/Inline | PGP/MIME | S/MIME)

After you enter all necessary information, the campaign will be created in local SQLite database.
You will be prompted to enter the passphrase for your PGP or S/MIME key.
After that, emails to security contacts will be signed, dispatched, and the meta-information will be stored
in the database. You will see a confirmation message and an ID of created campaign.

You can modify email templates that are used to generate emails. They are located in **mail_templates** folder.

The security contact receives i.e. following email from MailPing host::

    Dear service administrator,


    you receive this email because you are registered as security contact for
    the service(s) ID=172,188 in the Helmholtz AAI.

    In order to measure the responsiveness of security contacts within the
    Helmholtz AAI, we kindly ask you to perform one of the following
    actions:

    - Visit the following link in order to confirm that you received this
      email: https://mailping.data.kit.edu/confirmation?code=2d88266e2f344d7c5c101f9ade74e6a
    - Reply to this email including this message in the reply section, so that
      we can parse the verification link.


    Kind regards,
    Sirtfi Mail Ping


**Gather responses**

This step is done by MailPing.

If the security contact decides to use the confirmation link from
the ping email, the information about the response will be passed through the *public-api/confirmation/{code}* endpoint into
the database.

If the security contact replies on the ping email, the confirmation code will be parsed from the email body
and the response will be directly stored in the database. After that, the MailPing host sends the confirmation email
to inform that the reply has been received. The security contact will receive i.e. the following confirmation::

    Dear service administrator,


    thank you for your reply. Your response (after 1 day 12:37:59) for service(s) ID 172,188
    has been successfully recorded.


    Kind regards,
    Sirtfi Mail Ping

In case the confirmation code has been already used (ping already confirmed), an appropriate email will be send
to the security contact (cf. *mail_templates.already_replied_template.txt*).

If the confirmation code is invalid / could not be parsed / does not exist,
an appropriate email will be sent (cf. *mail_templates.verification_not_found.txt*).


**Evaluate responses**

To evaluate responses open the website that is hosted on the same machine as the command line tool and the database.

After authorisation with OpenID Connect you will see the overview of available campaigns:

.. image:: images/campaigns.png
   :width: 700

Select the campaign to see the detailed information:

.. image:: images/campaigns_id.png
   :width: 700

.. image:: images/plots.png
   :width: 700

Go to *Covered services* => *Show more* to see the pings grouped by services.

**Reping contacts**

If you want to 'reping' security contacts that have not responded yet, run the following command::

    reping

You will be asked to enter the ID of campaign where you want to initiate the reping.

Security contacts that have not responded to the previous ping(s) within the given campaign yet, will
get a reminder, i.e.::

    Dear service administrator,


    this is a friendly reminder of the previous mail sent on 2020/12/07 10:00:23:
    ...

Another option is to use the 'Reping' button on the campaign webpage. But there, only the reping without
signing is possible. The reason is that we do not want to ask for the PGP/SMIME key passphrase on the website.
