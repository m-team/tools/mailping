"""
Used by postfix to process incoming emails on mailping host.
Parses the confirmation code from incoming emails and sends back the confirmation email.
If confirmation code is invalid/not found/already used, sends an appropriate response.
"""
from datetime import datetime
import sys
import re
import os
from sirtfi_mailping.core.mailsender import Mailsender, SignMode
from sirtfi_mailping.core.model.ping import Ping, ReactionType
from sirtfi_mailping.core.metrics import ResponseTime
from sirtfi_mailping.config import reply_subject, datetime_format, confirmation_link_regex
from sirtfi_mailping.core.utils import represent_response_type, represent_time


sender = sys.argv[1]


def send_confirmation(ping):
    """
    Sends confirmation email to security contact

    """
    reply_template = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  "mail_templates/confirmation_template.txt")
    with open(reply_template, 'r') as reply_template:
        reply_message = reply_template.read().format(response_time=represent_time(ResponseTime(ping).compute()),
                                                     services=repr(ping.services).replace('[', '').replace(']', ''))
    Mailsender(sender, reply_subject, reply_message).send_signed_mail(SignMode.PLAIN.value)


def send_already_replied_mail(ping):
    """
    Sends an appropriate reply to security contact in case the ping has been already confirmed

    """
    already_replied_template = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                            "mail_templates/already_replied_template.txt")
    with open(already_replied_template, 'r') as already_replied_template:
        reply_message = already_replied_template.read().format(
            reaction_type=represent_response_type(ping.reaction_type),
            reacted_at=ping.reacted_at)
    Mailsender(sender, reply_subject, reply_message).send_signed_mail(SignMode.PLAIN.value)


def send_verification_not_found():
    """
    Sends an appropriate reply to security contact in case the confirmation code has not been found or it is invalid

    """
    verification_not_found_template = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                   "mail_templates/verification_not_found_template.txt")
    with open(verification_not_found_template, 'r') as verification_not_found_template:
        reply_message = verification_not_found_template.read()
    Mailsender(sender, reply_subject, reply_message).send_signed_mail(SignMode.PLAIN.value)


def process_email():
    received_email = ""
    for line in sys.stdin:
        received_email += line.replace("=\n", "").replace("3D", "")

    validation_code = re.search(confirmation_link_regex, received_email)
    if validation_code:
        validation_code = validation_code.group(0)

    if validation_code is not None:
        ping = Ping.get_by_validation_code(validation_code)
        if ping is not None:
            if ping.reaction_type == ReactionType.WEBSITE.value or ping.reaction_type == ReactionType.EMAIL_REPLY.value:
                send_already_replied_mail(ping)
            else:
                ping.reacted_at = datetime.now().strftime(datetime_format)
                ping.reaction_type = ReactionType.EMAIL_REPLY.value
                ping.reaction_sender = sender
                ping.update()
                send_confirmation(ping)
        else:
            send_verification_not_found()
    else:
        send_verification_not_found()
