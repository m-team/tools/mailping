"""
Stores different configuration values for mailping:

- **datetime_format**: specifies the string format of datetime objects within mailping
- **confirmation_code_regex**: regular expression for confirmation codes
- **confirmation_link_regex**: regular expression for confirmation links
- **ping_subject**: email subject used for pings
- **reply_subject**: email subject used for replies on incoming emails
- **mail_pattern**: regular expression for validating email addresses
"""
datetime_format = "%d/%m/%Y %H:%M:%S"
confirmation_code_regex = "^[a-f0-9]*$"
confirmation_link_regex = "(?<=https:\/\/mailping.data.kit.edu\/confirmation\?code=).*.+?(?=)"
ping_subject = "Sirtfi Security Contact Ping"
reply_subject = "Re: Sirtfi Security Contact Ping"
mail_pattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?" \
                            "(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"

