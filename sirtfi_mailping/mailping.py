"""
Entry-point for 'mailping' command. Uses 'click' to implement command line interface.
"""
import click
import secrets
import os

from click.exceptions import BadOptionUsage

from sirtfi_mailping.core.contacts_file_handler import ContactsHandler
from sirtfi_mailping.core.model.campaign import Campaign
from sirtfi_mailping.core.model.ping import Ping
from datetime import datetime

from sirtfi_mailping.core.mailsender import SignMode, Mailsender
from sirtfi_mailping.config import ping_subject, datetime_format

ping_template = os.path.join(os.path.dirname(os.path.abspath(__file__)), "mail_templates/ping_template.txt")


@click.command()
@click.option('--contacts', prompt='Path to contacts file', help="Path to contacts file")
@click.option('--campaign', prompt='Campaign name', help="Campaign name")
@click.option('--sign_mode', prompt='Sign mode(PGP/MIME | PGP/Inline | S/MIME)',
              help="Signing mode: PGP/Inline or PGP/MIME or S/MIME")
def mailping(contacts, campaign, sign_mode):
    """
    Command line program for creating and sending ping campaigns.

    :param contacts: path to contacts file
    :type contacts: str
    :param campaign: campaign name
    :type campaign: str
    :param sign_mode: 'PGP/Inline' | 'PGP/MIME' | 'S/MIME'
    :type sign_mode: str

    :return None
    """
    if sign_mode == "PGP/Inline":
        sign_mode = SignMode.PGP_INLINE.value
    elif sign_mode == "PGP/MIME":
        sign_mode = SignMode.PGP_MIME.value
    elif sign_mode == "S/MIME":
        sign_mode = SignMode.S_MIME.value
    else:
        raise BadOptionUsage("sign_mode", "Wrong signing mode given")

    try:
        addresses = ContactsHandler().extract_contacts(contacts)
    except FileNotFoundError as e:
        raise BadOptionUsage("campaign", e)
    click.echo("Successful loaded {} mail addresses".format(str(len(addresses))))

    # store campaign
    created_at = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    campaign = Campaign(campaign, created_at, sign_mode)
    campaign.store()

    successful_pings = 0
    for contact in addresses.items():
        validation_code = secrets.token_hex(16)
        subject = ping_subject
        with open(ping_template, 'r') as email_template:
            message = email_template.read().format(services=repr(contact[1]).replace('{', '').replace('}', ''),
                                                   code=validation_code)
        sent = Mailsender(contact[0], subject, message).send_signed_mail(sign_mode)
        # store meta information about ping
        if sent:
            sent_at = datetime.now().strftime(datetime_format)
            ping = Ping(campaign.campaign_id, contact[0], contact[1], sent_at, validation_code)
            ping.store()
            successful_pings += 1
    click.echo("Successful pinged {} mail addresses".format(str(successful_pings)))
    click.echo("Campaign ID: {}".format(str(campaign.campaign_id)))


if __name__ == '__main__':
    mailping()

