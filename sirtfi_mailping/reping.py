"""
Entry-point for 'reping' command. Uses 'click' to implement command line interface.
"""
import click

from sirtfi_mailping.core.model.campaign import Campaign
from sirtfi_mailping.core.reping import Reping
from click.exceptions import BadParameter


@click.command()
@click.option('--campaign_id', prompt='Campaign id', help="Campaign id")
def reping(campaign_id):
    """
    Command line program for initiating the 'reping' process. Checks whether the campaign with given ID exists or not.


    :param campaign_id: unique Campaign ID
    :type campaign_id: int
    """
    campaign = Campaign.get_campaign_by_id(int(campaign_id))
    if campaign is None:
        raise BadParameter("Campaign with given ID does not exist")
    repings = Reping(campaign).reping()
    if repings[0] == 0:
        click.echo("All contacts already replied")
    else:
        click.echo("Successful repinged {} mail addresses".format(repings[0]))


if __name__ == '__main__':
    resend()

