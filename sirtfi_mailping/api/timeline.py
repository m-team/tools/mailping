"""
Handles /api/campaigns/{campaign_id}/timeline request
"""
import json

from sirtfi_mailping.core.metrics import ComputeServices, ComputeTimeline, CountServices
from sirtfi_mailping.core.model.campaign import Campaign
from sirtfi_mailping.core.model.ping import Ping

from aiohttp import web


async def handle_timeline(request):
    """
        Handles the following request::

            GET /api/campaigns/{campaign_id}/timeline

        :return: information needed to visualise responses on 'pings' over the time

        - Example response::

            HTTP Status Code: 200
                {
                    “payload”:[{
                         “responded_at_relative”: 2500,	    // relative response time(x-axis) in sec
                         “unique_services_acc”: 7 	        // accumulated unique services(y-axis)

                         “services”: [12,13,14,15]
                         “contact_email”: “test@test.com”
                         “response_duration”: “0:17:10”
                         “responded_at”: “2020/07/15 10:10:59”
                         “response_type”: “confirmation link”
                        }
                    ]
                }

        - Example response by invalid campaign_id::

            HTTP Status Code: 404
                {
                    campaign not found
                }
    """
    campaign = Campaign.get_campaign_by_id(request.match_info['campaign_id'])
    if campaign is None:
        return web.Response(text="campaign not found", status=404)

    pings = Ping.get_by_campaign_id(campaign.campaign_id)
    timeline = ComputeTimeline(pings).compute()
    response = {"boundaries": {"x": timeline[0],
                               "y": CountServices(pings).compute()},
                "payload": timeline[1]}
    return web.Response(text=json.dumps(response), status=200, content_type="application/json")
