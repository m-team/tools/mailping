"""
Handles /public-api/confirmation/{code} request
"""
from datetime import datetime
import json

from aiohttp import web
import re

from sirtfi_mailping.config import confirmation_code_regex, datetime_format
from sirtfi_mailping.core.metrics import ResponseTime
from sirtfi_mailping.core.model.ping import Ping, ReactionType
from sirtfi_mailping.core.utils import represent_response_type, represent_time


async def handle_confirmation(request):
    """
    Handles the following request for 'ping' confirmation::

        GET /public-api/confirmation/{code}

    :return: confirmation response

    - Example response by successful confirmation::

        HTTP Status Code: 200
            {
                “status”: “success”,
                “payload”: {
                    “response_duration”: “1 day 17:32:59”,
                    “service_ids”: [14, 15]
                }
            }

    - Example response by already confirmed 'ping'::

        HTTP Status Code: 200
            {
                “status”: “not_modified”,
                “payload”: {
                    “reply_method”: “email reply”,
                    “responded_at”: "2020/07/10 13:02:59"
                }
            }

    - Example response by invalid confirmation code::

        HTTP Status Code: 400
            {
                “status”: “bad_request”
            }
    """
    confirmation_code = request.match_info['code']
    if confirmation_code is None or not re.search(confirmation_code_regex, confirmation_code):
        return web.Response(status=400)
    ping = Ping.get_by_validation_code(confirmation_code)
    if ping is not None:
        if ping.reaction_type == ReactionType.WEBSITE.value or ping.reaction_type == ReactionType.EMAIL_REPLY.value:
            response = dict()
            response["status"] = "not_modified"
            response["payload"] = {"reply_method": represent_response_type(ping.reaction_type),
                                   "responded_at": represent_time(ping.reacted_at)}
            return web.Response(text=json.dumps(response), status=200, content_type="application/json")
        else:
            ping.reacted_at = datetime.now().strftime(datetime_format)
            ping.reaction_type = ReactionType.WEBSITE.value
            ping.update()
            response = dict()
            response["status"] = "success"
            response["payload"] = {"response_duration": represent_time(ResponseTime(ping).compute()),
                                   "service_ids": ping.services}
            return web.Response(text=json.dumps(response), content_type="application/json")
    else:
        response = dict()
        response["status"] = "bad_request"
        return web.Response(text=json.dumps(response), status=400, content_type="application/json")
