"""
Handles /api/campaigns/{campaign_id}/services request
"""
from sirtfi_mailping.core.metrics import ComputeServices
from sirtfi_mailping.core.model.campaign import Campaign
from sirtfi_mailping.core.model.ping import Ping, ReactionType
from sirtfi_mailping.core.utils import represent_time, represent_response_type

from aiohttp import web
import json


async def handle_services(request):
    """
        Handles the following request::

            GET /api/campaigns/{campaign_id}/services

        Serves information about 'pings' grouped by services.

        :return: information about 'pings' grouped by services

        - Example response::

            HTTP Status Code: 200
                {
                    “campaign_name”: “Campaign Name”,
                    “campaign_id”: 1,
                    “services”: [{
                        “service_id”: 1,
                        “contacts_details”: [
                            {
                                “contact_email”: “test@gmail.com”,
                                “response_duration”: “10:10” | null,
                                “responded_at”: “2020/07/12 10:10:59” | null,
                                “response_type”: “email reply” | null,
                                “reply_address”: “test@gmail.com” | null
                            }, ... ]
                    }
                }

        - Example response by invalid campaign_id::

            HTTP Status Code: 404
                {
                    campaign not found
                }
    """
    campaign = Campaign.get_campaign_by_id(request.match_info['campaign_id'])
    if campaign is None:
        return web.Response(text="campaign not found", status=404)

    result = {"campaign_id": campaign.campaign_id,
              "campaign_name": campaign.campaign_name}

    pings = Ping.get_by_campaign_id(campaign.campaign_id)
    if pings is not None:
        services = ComputeServices(pings).compute()
        result["services"] = list()
        for service_id in services:
            contact_details = list()
            for contact in services[service_id]:
                contact_details.append({
                    "contact_email": contact[0],
                    "response_duration": represent_time(contact[1]),
                    "responded_at": represent_time(contact[2]),
                    "response_type": represent_response_type(contact[3]),
                    "reply_address": contact[4]
                })
            result["services"].append({
                "service_id": service_id,
                "contact_details": contact_details
            })

    return web.Response(text=json.dumps(result), status=200, content_type="application/json")
