"""
Handles /campaigns/all, /campaigns/{id} requests
"""
from sirtfi_mailping.core.metrics import AverageResponseTime, PingCounts, FirstResponse, CountServices
from sirtfi_mailping.core.model.campaign import Campaign
from sirtfi_mailping.core.model.ping import Ping
from sirtfi_mailping.core.utils import represent_time, represent_sign_method

from aiohttp import web
import json


async def handle_campaigns(request):
    """
    Handles the following request::

        GET /api/campaigns

    :return: list of available campaigns

    - Example response::

        HTTP Status Code: 200
            {
                “campaign_id”: 1,
                “campaign_name”: “Campaign name”,
                “created_at”: “2020/07/20 10:10”,
                “sign_method”: “PGP/MIME”
            }
    """
    campaigns = Campaign.get_campaigns()
    result = list()
    for campaign in campaigns:
        result.append({"campaign_id": campaign.campaign_id,
                       "campaign_name": campaign.campaign_name,
                       "created_at": campaign.created_at,
                       "sign_method": represent_sign_method(campaign.sign_method)})
    return web.Response(text=json.dumps(result), status=200, content_type="application/json")


async def handle_campaign_by_id(request):
    """
    Handles the following request::

        GET /api/campaigns/{campaign_id}

    :return: information about the campaign specified by campaign_id

    - Example response::

        HTTP Status Code: 200
            {
                “campaign_id”: 1,
                “campaign_name”: “Campaign name”,
                “created_at”: “2020/07/20 10:10”,
                “sign_method”: “PGP/MIME”,
                “average_response_duration”: “23:20:59”,
                “pings_count”: 150,
                “responded_pings_reply_count”: 10,                  // email responses
                “responded_pings_link_count”: 15,                   // confirmation link responses
                “not_responded_pings_count”: 125,
                “first_response_at”:  null | ”10/10/10 10:10:10”,
                “services_count”: 220                               // number of covered services
            }

    - Example response by invalid campaign_id::

        HTTP Status Code: 404
            campaign not found
    """
    campaign = Campaign.get_campaign_by_id(request.match_info['campaign_id'])
    if campaign is None:
        return web.Response(text="campaign not found", status=404)

    result = {"campaign_id": campaign.campaign_id,
              "campaign_name": campaign.campaign_name,
              "created_at": campaign.created_at,
              "sign_method": represent_sign_method(campaign.sign_method)}

    pings = Ping.get_by_campaign_id(campaign.campaign_id)
    if pings is not None:
        result["average_response_duration"] = represent_time(AverageResponseTime(pings).compute())
        ping_counts = PingCounts(pings).compute()
        result["pings_count"] = ping_counts[0]
        result["responded_pings_reply_count"] = ping_counts[1]
        result["responded_pings_link_count"] = ping_counts[2]
        result["not_responded_pings_count"] = ping_counts[3]
        result["first_response_at"] = represent_time(FirstResponse(pings).compute())
        result["services_count"] = CountServices(pings).compute()

    return web.Response(text=json.dumps(result), status=200, content_type="application/json")
