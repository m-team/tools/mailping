"""
Defines API endpoints
"""
from sirtfi_mailping.api.confirmation import handle_confirmation
from sirtfi_mailping.api.campaigns import handle_campaigns, handle_campaign_by_id
from sirtfi_mailping.api.services import handle_services
from sirtfi_mailping.api.reping import handle_reping
from sirtfi_mailping.api.timeline import handle_timeline


def setup_routes(app):
    """
    Specifies API endpoints and their handlers

    :param app: aiohttp application
    :return: None
    """
    app.router.add_get('/public-api/confirmation/{code}', handle_confirmation)
    app.router.add_get('/api/campaigns', handle_campaigns)
    app.router.add_get('/api/campaigns/{campaign_id}', handle_campaign_by_id)
    app.router.add_get('/api/campaigns/{campaign_id}/services', handle_services)
    app.router.add_get('/api/campaigns/{campaign_id}/reping', handle_reping)
    app.router.add_get('/api/campaigns/{campaign_id}/timeline', handle_timeline)
