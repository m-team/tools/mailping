"""
Handles /api/campaigns/{campaign_id}/reping request
"""
import json

from sirtfi_mailping.core.mailsender import SignMode
from sirtfi_mailping.core.model.campaign import Campaign
from sirtfi_mailping.core.reping import Reping

from aiohttp import web


async def handle_reping(request):
    """
    Handles the following request for initiating a 'reping' -
    sending reminder emails to contacts that did not responded::

        GET /api/campaigns/{campaign_id}/reping

    :return: information about 'reping'

    - Example response by successful 'reping'::

        HTTP Status Code: 200
            {
                “status”: “success”,
                “payload”: {
                    “repings”: 7,                   // amount of 'repinged' contacts
                    “reping_services”: [14, 15]     // services that have been 'repinged'
                }
            }

    - Example response by invalid campaign_id::

        HTTP Status Code: 404
            {
                “error”: “Campaign not found”
            }
    """
    campaign = Campaign.get_campaign_by_id(request.match_info['campaign_id'])
    if campaign is None:
        error = {
            "error": "Campaign not found"
        }
        return web.Response(text=json.dumps(error), status=404)
    campaign.set_sign_method(SignMode.PLAIN.value)
    reping = Reping(campaign).reping()

    response = {"status": "success",
                "payload": {
                    "repings": reping[0],
                    "reping_services": list(reping[1])
                }}
    return web.Response(text=json.dumps(response), status=200, content_type="application/json")
