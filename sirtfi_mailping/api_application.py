"""
Initialises the aiohttp application that serves API requests
"""
from aiohttp import web
from sirtfi_mailping.api.routes import setup_routes


app = web.Application()
setup_routes(app)
