"""
Contains functionality for sending signed emails using mutt along with gpgme
"""
import subprocess
import os
from enum import Enum


class SignMode(Enum):
    """
    Enum that represents email signing method.
    """
    PGP_INLINE = 1
    PGP_MIME = 2
    S_MIME = 3
    PLAIN = 4


class Mailsender():
    """
    Implements methods for sending signed emails using subprocess to run mutt commands in batch mode.

    :param address: recipient address
    :type address: str
    :param subject: email subject
    :type subject: str
    :param address: plain email body
    :type address: str
    """
    def __init__(self, address, subject, message):
        self.address = address
        self.subject = subject
        self.message = message

        self.tempfile_name = F"/tmp/headerline-{os.geteuid()}.txt"

        with open (self.tempfile_name, "w") as output:
            output.write('Reply-To: mailping-0001@scc.kit.edu\n')

    def __send_pgp_mime_signed_mail(self):
        echo = subprocess.Popen(('echo', self.message), stdout=subprocess.PIPE)
        mutt = subprocess.Popen(('mutt',
                '-e', 'set crypt_autosign = yes',
                '-H', self.tempfile_name,
                '-s', self.subject, '--', self.address),
                                stdin=echo.stdout,
                                stderr=subprocess.PIPE)

        echo.stdout.close()
        out, err = mutt.communicate()
        os.unlink(self.tempfile_name)
        return mutt.returncode == 0

    def __send_s_mime_signed_mail(self):
        echo = subprocess.Popen(('echo', self.message), stdout=subprocess.PIPE)
        mutt = subprocess.Popen(('mutt',
                '-e', 'set smime_is_default=yes',
                '-e', 'set crypt_autosign=yes',
                '-H', self.tempfile_name,
                '-s', self.subject, '--', self.address),
                                stdin=echo.stdout,
                                stderr=subprocess.PIPE)

        echo.stdout.close()
        out, err = mutt.communicate()
        os.unlink(self.tempfile_name)
        return mutt.returncode == 0

    def __send_inline_signed_mail(self):
        echo = subprocess.Popen(('echo', self.message), stdout=subprocess.PIPE)
        gpg = subprocess.Popen(('gpg', '--armor', '--sign', '--pinentry-mode', 'loopback'),
                               stdin=echo.stdout, stdout=subprocess.PIPE)
        mutt = subprocess.Popen(('mutt',
                '-H', self.tempfile_name,
                '-s', self.subject, '--', self.address),
                                stdin=gpg.stdout,
                                stderr=subprocess.PIPE)

        echo.stdout.close()
        out, err = mutt.communicate()
        os.unlink(self.tempfile_name)
        return mutt.returncode == 0

    def __send_plain_mail(self):
        echo = subprocess.Popen(('echo', self.message), stdout=subprocess.PIPE)
        mutt = subprocess.Popen(('mutt',
                '-H', self.tempfile_name,
                '-s', self.subject, '--', self.address),
                                stdin=echo.stdout,
                                stderr=subprocess.PIPE)
        echo.stdout.close()
        out, err = mutt.communicate()
        os.unlink(self.tempfile_name)
        return mutt.returncode == 0

    def send_signed_mail(self, mode):
        """
        Sends signed or plain email using signing method specified in mode

        :param mode: signing mode
        :type mode: int

        :return: True if successful
        :rtype: bool
        """
        if mode == SignMode.PGP_INLINE.value:
            return self.__send_inline_signed_mail()
        elif mode == SignMode.PGP_MIME.value:
            return self.__send_pgp_mime_signed_mail()
        elif mode == SignMode.S_MIME.value:
            return self.__send_s_mime_signed_mail()
        elif mode == SignMode.PLAIN.value:
            return self.__send_plain_mail()
