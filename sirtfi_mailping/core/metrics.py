"""
Contains functionality for metric computations on Pings
"""
from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from sirtfi_mailping.config import datetime_format

from sirtfi_mailping.core.model.ping import ReactionType
from sirtfi_mailping.core.utils import represent_time, represent_response_type


class IPingMetric(ABC):
    """
    Represents an abstract metric which can be computed on a single Ping instance or a list of Pings
    """
    @abstractmethod
    def compute(self):
        """
        Computes metric values from Ping/list of Pings

        :return: computed values
        """
        pass


class ResponseTime(IPingMetric):
    """
    Response duration metric for a single Ping

    :param ping: Ping that has to be considered
    :type ping: Ping
    """
    def __init__(self, ping):
        self.ping = ping

    def compute(self):
        """
        Computes response duration based on sent_at and reacted_at timestamps

        :return: time difference between ping dispatching and response
        :rtype: datetime.timedelta
        """
        if self.ping.reacted_at is not None and self.ping.sent_at is not None:
            reacted_at = datetime.strptime(self.ping.reacted_at, datetime_format)
            sent_at = datetime.strptime(self.ping.sent_at, datetime_format)
            duration = reacted_at - sent_at
            return duration
        else:
            return None


class AverageResponseTime(IPingMetric):
    """
    Average response duration metric for a list of Pings

    :param pings: list of Pings that have to be considered
    :type pings: list
    """
    def __init__(self, pings):
        self.pings = pings

    def compute(self):
        """
        Computes average response duration based on ResponseTime metric of every Ping from the list

        :return: average response duration
        :rtype: datetime.timedelta
        """
        sum_response_time = timedelta()
        responded_pings_count = 0
        for ping in self.pings:
            response_time = ResponseTime(ping).compute()
            if response_time is not None:
                sum_response_time += response_time
                responded_pings_count += 1
        if responded_pings_count > 0:
            return sum_response_time / responded_pings_count
        else:
            return None


class PingCounts(IPingMetric):
    """
    Represents countable metrics on the list of Pings

    :param pings: list of Pings that have to be considered
    :type pings: list
    """
    def __init__(self, pings):
        self.pings = pings

    def compute(self):
        """
        Computes following metrics:

        - overall number of pings,
        - number of pings answered by email reply,
        - number of pings answered by confirmation link,
        - number of pings without response

        :return: list of int metrics
        :rtype: list
        """
        response_reply_count = 0
        response_link_count = 0
        for ping in self.pings:
            if ping.reaction_type is ReactionType.EMAIL_REPLY.value:
                response_reply_count += 1
            elif ping.reaction_type is ReactionType.WEBSITE.value:
                response_link_count += 1
        without_response_count = len(self.pings) - response_reply_count - response_link_count
        return [len(self.pings), response_reply_count, response_link_count, without_response_count]


class FirstResponse(IPingMetric):
    """
    Represents First Response metric

    :param pings: list of Pings that have to be considered
    :type pings: list
    """
    def __init__(self, pings):
        self.pings = pings

    def compute(self):
        """
        Computes the time of the first response within given Pings

        :return: timestamp of the first response
        :rtype: datetime or NONE
        """
        first_response = None
        for ping in self.pings:
            if ping.reacted_at is not None:
                reacted_at = datetime.strptime(ping.reacted_at, datetime_format)
                if first_response is None or reacted_at < first_response:
                    first_response = reacted_at
        if first_response is not None:
            return first_response.strftime(datetime_format)
        return first_response


class CountServices(IPingMetric):
    """
    Counts number of unique services that are covered by given Pings

    :param pings: list of Pings that have to be considered
    :type pings: list
    """
    def __init__(self, pings):
        self.pings = pings

    def compute(self):
        """
        Counts number of unique services that are covered by given Pings

        :return: number of covered services
        :rtype: int
        """
        services = set()
        for ping in self.pings:
            services.update(ping.services)
        return len(services)


class ComputeServices(IPingMetric):
    """
    Groups information about Pings by services.

    :param pings: list of Pings that have to be considered
    :type pings: list
    """
    def __init__(self, pings):
        self.pings = pings

    def compute(self):
        """
        Assembles information about given Pings grouping it by service_ids

        :return: mapping of unique services to the list of their Pings
        :rtype: dict
        """
        services = dict()
        for ping in self.pings:
            for service_id in ping.services:
                services[service_id] = list()
        for ping in self.pings:
            response_time = ResponseTime(ping).compute()
            for service_id in ping.services:
                services[service_id].append([ping.recipient_address, response_time,
                                             ping.reacted_at, ping.reaction_type, ping.reaction_sender])
        return services


class ComputeTimeline(IPingMetric):
    """
    Represents Timeline metric for the list of Pings

    :param pings: list of Pings that have to be considered
    :type pings: list
    """
    def __init__(self, pings):
        self.pings = pings

    def compute(self):
        """
        Computes values needed to represent the timeline:

        - reaction time relative to the ping dispatching time
        - accumulated number of covered services
        - additional information about the Ping

        :return: cf. API specification of /timeline request
        :rtype: dict
        """
        responded_pings = list()
        for ping in self.pings:
            if ping.reacted_at is not None:
                responded_pings.append(ping)
                reacted_at = datetime.strptime(ping.reacted_at, datetime_format)
                sent_at = datetime.strptime(ping.sent_at, datetime_format)
                ping.reacted_at_relative = (reacted_at - sent_at).total_seconds()

        responded_pings.sort(key=lambda x: x.reacted_at_relative)
        last_response_relative = responded_pings[-1].reacted_at_relative

        ping_points = list()
        unique_services = set()
        for ping in responded_pings:
            unique_services.update(ping.services)
            ping_points.append({
                "x": ping.reacted_at_relative,
                "y": len(unique_services),
                "services": ping.services,
                "contact_email": ping.recipient_address,
                "response_duration": represent_time(ResponseTime(ping).compute()),
                "responded_at": represent_time(ping.reacted_at),
                "response_type": represent_response_type(ping.reaction_type)
            })
        return [last_response_relative, ping_points]
