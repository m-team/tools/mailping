"""
Contains functionality for 'reping' command that can be initiated both from command line and API.
'reping' can be used to send reminder emails to security contacts that have not responded to previous pings
"""
import os

from sirtfi_mailping.core.mailsender import Mailsender
from sirtfi_mailping.core.model.ping import Ping
from sirtfi_mailping.core.utils import represent_time
from sirtfi_mailping.config import ping_subject

reping_template = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../mail_templates/reping_template.txt")


class Reping():
    """
    Represents 'reping' functionality for given campaign

    :param campaign: Campaign that should be 'repinged'
    :type campaign: Campaign
    """
    def __init__(self, campaign):
        self.campaign = campaign

    def reping(self):
        """
        Sends reminder emails to security contacts that have not responded yet

        :return: number of 'repings' that have been sent, list of service_ids that have been 'repinged'
        :rtype: list
        """
        pings = Ping.get_by_campaign_id(int(self.campaign.campaign_id))
        services = set()
        reping_count = 0
        for ping in pings:
            if ping.reaction_type is None:
                subject = ping_subject
                with open(reping_template, 'r') as email_template:
                    message = email_template.read().format(sent_at=represent_time(ping.sent_at),
                                                           services=repr(ping.services)
                                                           .replace('[', '').replace(']', ''),
                                                           code=ping.validation_code)
                sent = Mailsender(ping.recipient_address, subject, message).send_signed_mail(self.campaign.sign_method)
                if sent:
                    reping_count += 1
                    services.update(ping.services)
        return [reping_count, services]
