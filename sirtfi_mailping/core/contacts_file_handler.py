"""
Contains functionality for extracting and validating security contact emails from input file.

Every row of the input .txt file has following structure::

[Service-ID],[email address],...,[email address]

"""
import re
from sirtfi_mailping.config import mail_pattern


class ContactsHandler:
    """
    Extracts and validates security contacts from the input file
    """

    def __init__(self):
        self.addresses = dict()

    def __validate_email_address(self, email_address):
        if re.search(mail_pattern, email_address):
            return True

    def __process_line(self, line):
        line = line.split(",")
        service_id = line[0].rstrip()
        for address in line:
            address = address.rstrip()
            if self.__validate_email_address(address):
                if address in self.addresses:
                    self.addresses[address].add(int(service_id))
                else:
                    self.addresses[address] = set()
                    self.addresses[address].add(int(service_id))

    def extract_contacts(self, path_to_contacts_file):
        """
        Extracts security contact emails from input file

        :param path_to_contacts_file: path to file containing security contacts
        :type path_to_contacts_file: str
        :return: mapping of email addresses to the services
        :rtype: dict
        """
        with open(path_to_contacts_file, "r") as file:
            for line in file:
                self.__process_line(line)
        return self.addresses
