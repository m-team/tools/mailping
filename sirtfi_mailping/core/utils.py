"""
Represents helper methods for data representation including:

- time representation,
- response type representation,
- signing method representation
"""
from sirtfi_mailping.core.mailsender import SignMode
from sirtfi_mailping.core.model.ping import ReactionType


def represent_time(time):
    """
    Wrapper for datetime objects

    :param time: timestamp
    :type time: datetime

    :return: timestamp as string or None
    :rtype: string
    """
    if time is None:
        return None
    else:
        return str(time)


def represent_response_type(response_type):
    """
    Converts ReactionType enum to string representation

    :param response_type: value of ReactionType enum
    :type response_type: int

    :return: reaction type representation
    :rtype: string
    """
    if response_type == ReactionType.EMAIL_REPLY.value:
        response = "email reply"
    elif response_type == ReactionType.WEBSITE.value:
        response = "confirmation link"
    else:
        response = None
    return response


def represent_sign_method(sign_method):
    """
    Converts SignMode enum to string representation

    :param sign_method: value of SignMode enum
    :type sign_method: int

    :return: signing method representation
    :rtype: string
    """
    if sign_method == SignMode.PGP_INLINE.value:
        sign_mode = "PGP/Inline"
    elif sign_method == SignMode.PGP_MIME.value:
        sign_mode = "PGP/MIME"
    elif sign_method == SignMode.S_MIME.value:
        sign_mode = "S/MIME"
    else:
        sign_mode = None
    return sign_mode
