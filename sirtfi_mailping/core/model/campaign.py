"""
Contains model of a Campaign record
"""

from sirtfi_mailping.core.mailsender import SignMode
from sirtfi_mailping.core.database.database import execute_insert_query
from sirtfi_mailping.core.database.database import execute_select_query


class Campaign:
    """
    Represents model of a Campaign record. Uses Active Record pattern
    for data persistence

    :param campaign_name: indicates campaign name. Does not have to be unique.
    :type campaign_name: str
    :param created_at: contains timestamp of campaign creation
    :type created_at: str
    :parameter sign_method: represents signing method
    :type sign_method: int

    """

    def __init__(self, campaign_name, created_at, sign_method):
        self.campaign_id = -1
        self.campaign_name = campaign_name
        self.created_at = created_at
        self.sign_method = sign_method

    def store(self):
        """
        Persists Campaign object in database

        :return: None

        """
        sql = "INSERT INTO campaign (campaign_name, created_at, sign_method) VALUES (?, ?, ?)"
        parameters = (self.campaign_name, self.created_at, self.sign_method)
        self.campaign_id = execute_insert_query(sql, parameters)

    def set_sign_method(self, sign_method):
        """
        Sets signing method for campaign object

        :param sign_method: signing method
        :type sign_method: int

        :return: None
        """
        self.sign_method = sign_method

    @staticmethod
    def get_campaigns():
        """
        Static method that returns list of available campaigns

        :return: list of existing campaigns
        :rtype: list or None
        """
        sql = "SELECT * FROM campaign"
        campaigns_db = execute_select_query(sql, None)
        campaigns = list()
        if len(campaigns_db) > 0:
            for campaign_db in campaigns_db:
                campaign = Campaign(campaign_db[1], campaign_db[2], campaign_db[3])
                campaign.campaign_id = campaign_db[0]
                campaigns.append(campaign)
            return campaigns
        else:
            return None

    @staticmethod
    def get_campaign_by_id(campaign_id):
        """
        Gets Campaign by its unique ID

        :param campaign_id: ID of requested Campaign
        :type campaign_id: int

        :return: Campaign specified by campaign_id
        :rtype: Campaign or None
        """
        sql = "SELECT * FROM campaign WHERE campaign_id = ?"
        campaigns_db = execute_select_query(sql, [int(campaign_id)])
        if len(campaigns_db) == 1:
            for campaign_db in campaigns_db:
                campaign = Campaign(campaign_db[1], campaign_db[2], campaign_db[3])
                campaign.campaign_id = campaign_db[0]
            return campaign
        else:
            return None
