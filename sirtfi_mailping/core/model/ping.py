"""
Contains model of a Ping record
"""

from enum import Enum
import json

from sirtfi_mailping.core.database.database import execute_insert_query, execute_select_query, execute_update_query


class ReactionType(Enum):
    """
    Enum that represents two possible reaction types:

    - **EMAIL_REPLY**: email reply on the ping mail
    - **WEBSITE**: click on a confirmation link from the ping mail
    """
    EMAIL_REPLY = 1
    WEBSITE = 2


class Ping:
    """
    Represents model of a Ping record. Uses Active Record pattern
    for data persistence

    :param campaign_id: unique Campaign ID
    :type campaign_id: int
    :param recipient_address: security contact email
    :type recipient_address: str
    :param services: service IDs of the security contact
    :type services: list
    :param sent_at: timestamp indicating when the ping email has been sent
    :type sent_at: str
    :param validation_code: unique confirmation code
    :type validation_code: str

    """
    def __init__(self, campaign_id, recipient_address, services, sent_at, validation_code):
        self.ping_id = -1
        self.campaign_id = campaign_id
        self.recipient_address = recipient_address
        self.services = list(services)
        self.sent_at = sent_at
        self.validation_code = validation_code
        self.reacted_at = str()
        self.reaction_type = -1
        self.reaction_sender = str()

    def store(self):
        """
        Persists Ping object in database

        :return: None
        """
        sql = "INSERT INTO ping (campaign_id, recipient_address, services, sent_at, validation_code) " \
              "VALUES (?, ?, ?, ?, ?)"
        parameters = (self.campaign_id, self.recipient_address, json.dumps(self.services),
                      self.sent_at, self.validation_code)
        self.ping_id = execute_insert_query(sql, parameters)

    def update(self):
        """
        Updates Ping object in database

        :return: None
        """
        sql = "UPDATE ping SET reacted_at = ?, reaction_type = ?, reaction_sender = ? WHERE ping_id = ?"
        parameters = [self.reacted_at, self.reaction_type, self.reaction_sender, self.ping_id]
        execute_update_query(sql, parameters)

    @staticmethod
    def get_by_validation_code(validation_code):
        """
        Gets Ping by unique confirmation code

        :param validation_code: confirmation code of the Ping
        :type validation_code: str
        :return: Ping specified by confirmation code
        :rtype: Ping or None
        """
        sql = "SELECT * FROM ping WHERE validation_code = ?"
        parameters = [str(validation_code)]
        pings = execute_select_query(sql, parameters)
        if len(pings) == 1:
            ping_db = pings[0]
            ping = Ping(ping_db[1], ping_db[2], json.loads(ping_db[3]),
                        ping_db[4], ping_db[5])
            ping.reacted_at = ping_db[6]
            ping.reaction_type = ping_db[7]
            ping.reaction_sender = ping_db[8]
            ping.ping_id = ping_db[0]
            return ping
        else:
            return None

    @staticmethod
    def get_by_campaign_id(campaign_id):
        """
        Gets Pings from given Campaign

        :param campaign_id: unique Campaign ID
        :type campaign_id: int
        :return: Pings from given Campaign ID
        :rtype: list of Pings
        """
        sql = "SELECT * FROM ping WHERE campaign_id = ?"
        parameters = [int(campaign_id)]
        pings_db = execute_select_query(sql, parameters)
        if len(pings_db) > 0:
            pings = list()
            for ping_db in pings_db:
                ping = Ping(ping_db[1], ping_db[2], json.loads(ping_db[3]),
                            ping_db[4], ping_db[5])
                ping.reacted_at = ping_db[6]
                ping.reaction_type = ping_db[7]
                ping.reaction_sender = ping_db[8]
                ping.ping_id = ping_db[0]
                pings.append(ping)
            return pings
        else:
            return None

