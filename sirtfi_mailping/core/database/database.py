"""
Wraps queries to SQLite
"""
import sqlite3
import os

database_path = "/var/lib/sirtfi_mailping/mailping.db"


def execute_insert_query(sql, parameters):
    """
    Executes 'INSERT' query for SQLite

    :param sql: Insert SQL query
    :type sql: str
    :param parameters: query parameters
    :type parameters: tuple

    :return: ID of created row
    :rtype: int
    """
    connection = sqlite3.connect(database_path)
    cursor = connection.cursor()
    cursor.execute(sql, parameters)
    connection.commit()
    connection.close()
    return cursor.lastrowid


def execute_select_query(sql, parameters):
    """
    Executes 'SELECT' query for SQLite

    :param sql: Select SQL query
    :type sql: str
    :param parameters: query parameters
    :type parameters: tuple

    :return: list of fetched rows
    :rtype: list
    """
    connection = sqlite3.connect(database_path)
    cursor = connection.cursor()
    if parameters is None:
        cursor.execute(sql)
    else:
        cursor.execute(sql, parameters)
    result = cursor.fetchall()
    connection.close()
    return result


def execute_update_query(sql, parameters):
    """
    Executes 'UPDATE' query for SQLite

    :param sql: Update SQL query
    :type sql: str
    :param parameters: query parameters
    :type parameters: tuple

    :return: None
    """
    connection = sqlite3.connect(database_path)
    cursor = connection.cursor()
    cursor.execute(sql, parameters)
    connection.commit()
    connection.close()

