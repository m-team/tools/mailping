#!/bin/bash

DB_FILE=/var/lib/sirtfi_mailping/mailping.db
DB_USER="root"
DB_GROUP="m-team"

rm -f ${DB_FILE}


echo "
CREATE TABLE IF NOT EXISTS "campaign" (
	"campaign_id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"campaign_name"	TEXT NOT NULL,
	"created_at"	TEXT NOT NULL,
	"sign_method"	INTEGER NOT NULL
);
CREATE TABLE IF NOT EXISTS "ping" (
	"ping_id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"campaign_id"	INTEGER NOT NULL,
	"recipient_address"	TEXT NOT NULL,
	"services"	TEXT NOT NULL,
	"sent_at"	TEXT,
	"validation_code"	TEXT NOT NULL UNIQUE,
	"reacted_at"	TEXT,
	"reaction_type"	INTEGER,
	"reaction_sender"	TEXT,
	FOREIGN KEY("campaign_id") REFERENCES "campaign"("campaign_id")
);
" | sqlite3 ${DB_FILE}

chmod 660 ${DB_FILE}
chown ${DB_USER}:${DB_GROUP} ${DB_FILE}
