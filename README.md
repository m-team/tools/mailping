# Sirtfi MailPing

**Documentation: https://mailping.data.kit.edu/docs**

MailPing is a tool that helps to keep security contact lists up-to-date
by performing and evaluating secure email 'ping' campaigns.
The tool aims to comply with [REFEDS Sirtfi framework](https://refeds.org/sirtfi)

MailPing allows to automatically send signed emails with unique confirmation codes
to security contacts of service providers by using
one of the following cryptographic signing methods:

- S/MIME
- PGP/MIME
- PGP/Inline

The tool automatically processes and stores the following types of replies:

- security contact replies on the signed 'ping' email,
- or clicks on the confirmation link provided in the 'ping' email.

MailPing stores the responses and visualises them on the website.

According to its tasks, MailPing consists of three parts:

1. Command line tool for creating a MailPing campaign and sending signed 'ping' emails to security contacts from an address list

2. Modules that process responses of security contacts either through confirmation link or through email reply

3. Website for visualising the campaign's progress and results, together with corresponding API endpoints on the backend.
